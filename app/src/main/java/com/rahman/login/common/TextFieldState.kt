package com.rahman.login.common

data class TextFieldState(
    val text :  String = "",
    val error: String? = null
)