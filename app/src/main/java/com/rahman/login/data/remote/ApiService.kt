package com.rahman.login.data.remote

import com.rahman.login.data.models.UserDto
import com.rahman.login.data.remote.request.AuthRequest
import com.rahman.login.data.remote.response.AuthResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    @POST("api/login")
    suspend fun loginUser(
        @Body loginRequest: AuthRequest
    ) : AuthResponse

    @GET("api/users/{id}")
    suspend fun getUserDetails(@Path("id") id : String) : UserDto

}