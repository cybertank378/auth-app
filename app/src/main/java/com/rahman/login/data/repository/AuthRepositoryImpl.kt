package com.rahman.login.data.repository

import android.content.SharedPreferences
import com.rahman.login.data.local.AuthPreferences
import com.rahman.login.data.models.UserDto
import com.rahman.login.data.remote.ApiService
import com.rahman.login.data.remote.request.AuthRequest
import com.rahman.login.domain.repository.AuthRepository
import com.rahman.login.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import retrofit2.HttpException
import java.io.IOException

class AuthRepositoryImpl(
    private val apiService: ApiService,
    private val preferences: AuthPreferences

) : AuthRepository{
    override suspend fun login(loginRequest: AuthRequest): Resource<Unit> {
       return try {
           val response = apiService.loginUser(loginRequest)
           preferences.saveAuthToken(response.token)
           Resource.Success(Unit)
       }catch (e: IOException){
           Resource.Error("${e.message}")
       }catch (e: HttpException){
           Resource.Error("${e.message}")
       }
    }

    override suspend fun detail(id : String): Resource<UserDto> {
        return try {
           val response = apiService.getUserDetails(id)
           Resource.Success(response)
        }catch (e: IOException){
            Resource.Error("${e.message}")
        }catch (e: HttpException){
            Resource.Error("${e.message}")
        }
    }


}