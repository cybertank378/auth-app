package com.rahman.login.data.models

import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.Flow

data class UserDto(

    @SerializedName("data")
    var data: Data? = Data(),
    @SerializedName("support")
    var support: Support? = Support(),
    var isLoading : Boolean = true

)