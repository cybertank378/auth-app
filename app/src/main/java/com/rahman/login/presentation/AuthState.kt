package com.rahman.login.presentation

data class AuthState(
    val isLoading: Boolean = false
)