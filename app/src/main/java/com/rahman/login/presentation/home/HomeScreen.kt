package com.rahman.login.presentation.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import coil.size.OriginalSize
import coil.size.Scale
import coil.transform.CircleCropTransformation
import com.rahman.login.common.UiEvents
import com.rahman.login.data.models.UserDto
import com.rahman.login.presentation.AuthViewModel
import com.rahman.login.ui.nav.Screen
import com.rahman.login.ui.theme.PurpleBg
import kotlinx.coroutines.flow.collectLatest

@Composable
fun HomeScreen(navigator: NavController, viewModel: AuthViewModel = hiltViewModel()) {


   val isLoad by viewModel.isLoad.observeAsState(false)
   val users by viewModel.userDetail.observeAsState(null)

   val scaffoldState = rememberScaffoldState()

   LaunchedEffect(key1 = true) {
      viewModel.eventFlow.collectLatest { event ->
         when (event) {
            is UiEvents.SnackbarEvent -> {
               scaffoldState.snackbarHostState.showSnackbar(
                  message = event.message,
                  duration = SnackbarDuration.Short
               )
            }
            is UiEvents.NavigateEvent -> {
               scaffoldState.snackbarHostState.showSnackbar(
                  message = "Data Loaded",
                  duration = SnackbarDuration.Short
               )
            }
         }
      }
   }

   Scaffold(
      scaffoldState = scaffoldState
   ) {
      it.apply {
         Modifier.padding(0.dp)
      }


      viewModel.userDetail()
      Column(
         modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(rememberScrollState()),
         horizontalAlignment = Alignment.CenterHorizontally
      ) {

         if(isLoad){
            CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterHorizontally))
         }


         val painter =
            // Gray Scale Transformation
            rememberAsyncImagePainter(       // Circle Crop Transformation
               ImageRequest.Builder(LocalContext.current)
                  .data(data = users?.data?.avatar)
                  .apply(block = fun ImageRequest.Builder.() {
                     size(45)
                     scale(Scale.FIT)
                     transformations(CircleCropTransformation())
                  }).build()
            )
         Image(

            painter = painter,
            contentDescription = null,
            modifier = Modifier.fillMaxSize(),
         )


         Text(
            modifier = Modifier.fillMaxWidth(),
            text = String.format("Hello %s %s",users?.data?.firstName ?:"", users?.data?.lastName ?:""),
            fontSize = 26.sp,
            color = Color.Black,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
         )
         Text(
            modifier = Modifier.fillMaxWidth(),
            text = "Welcome Back you've been missed",
            fontSize = 19.sp,
            color = Color.Black,
            fontWeight = FontWeight.Light,
            textAlign = TextAlign.Center
         )


         Text(
            modifier = Modifier.fillMaxWidth(),
            text = String.format("Email Address %s", users?.data?.email ?:""),
            fontSize = 19.sp,
            color = Color.Black,
            fontWeight = FontWeight.Light,
            textAlign = TextAlign.Center
         )



         Spacer(modifier = Modifier.height(16.dp))





      }
   }




}
