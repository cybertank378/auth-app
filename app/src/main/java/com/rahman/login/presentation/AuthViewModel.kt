package com.rahman.login.presentation

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rahman.login.common.TextFieldState
import com.rahman.login.common.UiEvents
import com.rahman.login.data.models.UserDto
import com.rahman.login.domain.use_case.LoginUseCase
import com.rahman.login.domain.use_case.UserUseCase
import com.rahman.login.ui.nav.Screen
import com.rahman.login.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val userUseCase: UserUseCase,
) : ViewModel() {


    val mData: UserDto = UserDto()
    private var _loginState = mutableStateOf(AuthState())


    private val _isLoad: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    val isLoad: LiveData<Boolean> get() = _isLoad

    private val _isUsers: MutableLiveData<UserDto> by lazy {
        MutableLiveData<UserDto>(null)
    }


    val userDetail: LiveData<UserDto> get() = _isUsers

    val loginState: State<AuthState> = _loginState
    private val _eventFlow = MutableSharedFlow<UiEvents>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val _firstName = mutableStateOf(TextFieldState())
    val firstName: State<TextFieldState> = _firstName

    fun setFirstName(value: String) {
        _firstName.value = firstName.value.copy(text = value)
    }

    private val _lastName = mutableStateOf(TextFieldState())
    val lastName: State<TextFieldState> = _lastName

    fun setLastName(value: String) {
        _lastName.value = lastName.value.copy(text = value)
    }

    private val _emailState = mutableStateOf(TextFieldState())
    val emailState: State<TextFieldState> = _emailState

    fun setEmail(value: String) {
        _emailState.value = emailState.value.copy(text = value)
    }

    private val _passwordState = mutableStateOf(TextFieldState())
    val passwordState: State<TextFieldState> = _passwordState

    fun setPassword(value: String) {
        _passwordState.value = passwordState.value.copy(text = value)
    }

    fun loginUser() {
        viewModelScope.launch {
            _loginState.value = loginState.value.copy(isLoading = false)

            val loginResult = loginUseCase(
                email = emailState.value.text,
                password = passwordState.value.text
            )

            _loginState.value = loginState.value.copy(isLoading = false)

            if (loginResult.emailError != null) {
                _emailState.value = emailState.value.copy(error = loginResult.emailError)
            }
            if (loginResult.passwordError != null) {
                _passwordState.value = passwordState.value.copy(error = loginResult.passwordError)
            }

            when (loginResult.result) {

                is Resource.Loading -> {
                    _loginState.value = loginState.value.copy(isLoading = true)
                }
                is Resource.Success -> {
                    _eventFlow.emit(
                        UiEvents.NavigateEvent(Screen.HomeScreen.route)
                    )

                }

                is Resource.Error -> {
                    UiEvents.SnackbarEvent(
                        loginResult.result.message ?: "Error!"
                    )
                }
                else -> {

                }
            }
        }
    }

    fun userDetail() {

        if (_isLoad.value == false){
            viewModelScope.launch(Dispatchers.IO) {
                _isLoad.postValue(true)
                val getUserResult = userUseCase("4")
                val mData = getUserResult.result.data as UserDto
                _isUsers.postValue(UserDto(data = mData.data, support = mData.support))
                _isLoad.postValue(false)
            }
        }

    }


}