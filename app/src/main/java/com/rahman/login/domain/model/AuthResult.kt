package com.rahman.login.domain.model

import com.rahman.login.data.models.UserDto
import com.rahman.login.util.Resource
import kotlinx.coroutines.flow.Flow

data class AuthResult(
    val passwordError: String? = null,
    val emailError : String? = null,
    val result: Resource<Unit>? = null
)

data class DetailResult(
    var result: Resource<UserDto>
)