package com.rahman.login.domain.repository

import com.rahman.login.data.models.UserDto
import com.rahman.login.data.remote.request.AuthRequest
import com.rahman.login.util.Resource
import kotlinx.coroutines.flow.Flow

interface AuthRepository {
    suspend fun login(loginRequest: AuthRequest):Resource<Unit>
    suspend fun detail(id: String):Resource<UserDto>
}