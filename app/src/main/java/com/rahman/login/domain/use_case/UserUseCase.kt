package com.rahman.login.domain.use_case

import com.rahman.login.domain.model.DetailResult
import com.rahman.login.domain.repository.AuthRepository

class UserUseCase(
    private val repository: AuthRepository
) {
    suspend operator fun invoke(id:String):DetailResult {

        return DetailResult(
            result = repository.detail(id)
        )
    }
}