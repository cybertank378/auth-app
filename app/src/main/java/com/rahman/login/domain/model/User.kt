package com.rahman.login.domain.model

import com.rahman.login.data.models.Data
import com.rahman.login.data.models.Support
import com.google.gson.annotations.SerializedName

data class User(
    var data: Data,
    var support: Support
)