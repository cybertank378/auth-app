package com.rahman.login

import android.os.Bundle
import android.view.Surface
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rahman.login.ui.nav.Screen
import com.rahman.login.presentation.AuthViewModel
import com.rahman.login.presentation.home.HomeScreen
import com.rahman.login.presentation.login.LoginScreen
import com.rahman.login.ui.theme.AuthAppTheme
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity: ComponentActivity() {

    private val authViewModel: AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AuthAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Navigation(authViewModel)
                }
            }
        }
    }

    @Composable
    fun Navigation(authViewModel: AuthViewModel) {
        val navController = rememberNavController()

        NavHost(navController = navController, startDestination = Screen.LoginScreen.route) {
            composable(
                route = Screen.LoginScreen.route,
                content = {
                    LoginScreen(
                        navigator = navController,
                        authViewModel
                    )
                })
            composable(
                route = Screen.HomeScreen.route,
                content = { HomeScreen(navigator = navController) })
        }
    }
}